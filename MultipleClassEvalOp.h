#ifndef ECF_CARTESIAN_MULTIPLECLASSEVALOP_H
#define ECF_CARTESIAN_MULTIPLECLASSEVALOP_H
#include <ECF.h>
#include <ECF_macro.h>
#include <EvaluateOp.h>

#include <string>
#include <vector>
typedef std::pair<std::vector<std::vector<double> > , std::vector<uint> > PairAllFeaturesAllLabels;
class MultipleClassEvalOp : public EvaluateOp{
private:
    std::vector<std::vector<double> > trainingInput;
    std::vector<uint> trainingOutputs;
    std::vector<std::vector<double> > testingInput;
    std::vector<uint> testingOutputs;
    std::string measureUsed;
    double softTargetBeta;
    double softTargetGamma;
    uint numberOfDifferentClasses;
    bool leadsWithId;
    bool softTarget;
public:
    MultipleClassEvalOp(){leadsWithId = false; softTarget = false;};
    void registerParameters(StateP stateP);
    bool initialize(StateP stateP);
    FitnessP evaluate(IndividualP individual);
};

#endif //ECF_CARTESIAN_MULTIPLECLASSEVALOP_H
