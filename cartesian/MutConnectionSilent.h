#ifndef ECF_CARTESIAN_MUTCONNECTIONSILENT_H
#define ECF_CARTESIAN_MUTCONNECTIONSILENT_H
#include <ECF_base.h>
namespace cartesian{
    class MutateConnectionSilent : public MutationOp
    {
    public:
        void registerParameters(StateP state);
        bool initialize(StateP state);
        bool mutate(GenotypeP gene);
    };
    typedef boost::shared_ptr<MutateConnectionSilent> MutateConnectionSilentP;
}
#endif //ECF_CARTESIAN_MUTCONNECTIONSILENT_H
