#ifndef ECF_CARTESIAN_SQRT_H
#define ECF_CARTESIAN_SQRT_H
#include "Function.h"
namespace cartesian{
    template <typename Container, typename Result>
    class Sqrt : public Function<Container,Result> {
    public:
        Sqrt();
        ~Sqrt(){};
        void evaluate(Container& container, Result& result);
    };

    template <typename Container, typename Result>
    Sqrt<Container,Result>::Sqrt()
    {
        this->name_ = "sqrt";
        this->numOfArgs_ = 1;
    }

    template <typename Container, typename Result>
    void Sqrt<Container,Result>::evaluate(Container& container, Result& result)
    {
        typename Container::iterator it = container.begin();
        if(std::isnan(*it) || std::isinf(*it)) {
            result = 1.0;
        }
        else if(*it < 0.0) {
            result = std::sqrt(std::abs(*it));
        }
        else {
            result = std::sqrt(*it);
        }
    }
}
#endif //ECF_CARTESIAN_SQRT_H
