#ifndef ECF_CARTESIAN_MUL_H
#define ECF_CARTESIAN_MUL_H
#include "Function.h"
namespace cartesian{
    template <typename Container, typename Result>
    class Mul : public Function<Container,Result> {
    public:
        Mul();
        ~Mul(){};
        void evaluate(Container& container, Result& result);
    };

    template <typename Container, typename Result>
    Mul<Container,Result>::Mul()
    {
        this->name_ = "*";
        this->numOfArgs_ = 2;
    }

    template <typename Container, typename Result>
    void Mul<Container,Result>::evaluate(Container& container, Result& result)
    {
        result = 1.0;
        for(typename Container::iterator it = container.begin(); it != container.begin() + this->numOfArgs_; it++) {
            result *= *it;
        }
    }
}
#endif //ECF_CARTESIAN_MUL_H
