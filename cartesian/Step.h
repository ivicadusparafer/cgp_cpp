#ifndef ECF_CARTESIAN_STEP_H
#define ECF_CARTESIAN_STEP_H
#include "Function.h"
namespace cartesian{
    template <typename Container, typename Result>
    class Step : public Function<Container,Result> {
    public:
        Step();
        ~Step(){};
        void evaluate(Container& container, Result& result);
    };

    template <typename Container, typename Result>
    Step<Container,Result>::Step()
    {
        this->name_ = "step";
        this->numOfArgs_ = 1;
    }

    template <typename Container, typename Result>
    void Step<Container,Result>::evaluate(Container& container, Result& result)
    {
        typename Container::iterator it = container.begin();
        if(*it > 0) {
            result = *it;
        }
        result = 0;
    }
}
#endif //ECF_CARTESIAN_STEP_H
