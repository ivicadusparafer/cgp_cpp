#ifndef ECF_CARTESIAN_CARTESIANCRXONEPOINT_H
#define ECF_CARTESIAN_CARTESIANCRXONEPOINT_H
#include <ECF_base.h>
namespace cartesian {
    class CartesianCrxOnePoint: public CrossoverOp
    {
    public:
        void registerParameters(StateP state);
        bool initialize(StateP state);
        bool mate(GenotypeP gen1, GenotypeP gen2, GenotypeP child);
    };
    typedef boost::shared_ptr<CartesianCrxOnePoint> CartesianCrxOnePointP;
}
#endif //ECF_CARTESIAN_CARTESIANCRXONEPOINT_H
