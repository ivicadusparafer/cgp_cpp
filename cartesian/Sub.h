#ifndef ECF_CARTESIAN_SUB_H
#define ECF_CARTESIAN_SUB_H
#include "Function.h"
namespace cartesian{
    template <typename Container, typename Result>
    class Sub : public Function<Container,Result> {
    public:
        Sub();
        ~Sub(){};
        void evaluate(Container& container, Result& result);
    };

    template <typename Container, typename Result>
    Sub<Container,Result>::Sub()
    {
        this->name_ = "-";
        this->numOfArgs_ = 2;
    }

    template <typename Container, typename Result>
    void Sub<Container,Result>::evaluate(Container& container, Result& result) {
        result = *(container.begin());
        for (typename Container::iterator it = container.begin() + 1; it != container.begin() + this->numOfArgs_; it++) {
            result -= *it;
        }
    }
}
#endif //ECF_CARTESIAN_SUB_H
