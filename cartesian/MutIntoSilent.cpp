#include <set>
#include "MutIntoSilent.h"
#include "Cartesian_genotype.h"
#include "FunctionSet.h"
namespace cartesian{
    void MutateIntoSilent::registerParameters(StateP state)
    {
        myGenotype_->registerParameter(state, "mut.tosilent", (voidP) new double(0), ECF::DOUBLE);
    }

    bool MutateIntoSilent::initialize(StateP state)
    {
        voidP sptr = myGenotype_->getParameterValue(state, "mut.tosilent");
        probability_ = *((double*)sptr.get());
        return true;
    }

    bool MutateIntoSilent::mutate(GenotypeP gene)
    {
        Cartesian* cartesian = (Cartesian*) gene.get();
        //First random output is chosen. Trail which decides the output will be mutated.
        uint whichOutput = cartesian->get_random_int(0, cartesian->nOutputs - 1);
        std::vector<uint>  activeTrail = cartesian->getActiveTrail(whichOutput);
        uint whoIsMutated = activeTrail[cartesian->get_random_int(0, activeTrail.size() - 1)];
        //Now, indexes of silent nodes are needed. Only silent nodes before whoIsMutated are needed.
        std::set<uint> allActiveIndexes;
        std::vector<std::vector<uint> >  allTrails = cartesian->getActiveTrails();
        for(uint i = 0; i < allTrails.size(); i++) {
            for(uint j = 0; j < allTrails[i].size(); j++) {
                if(allTrails[i][j] < whoIsMutated) {
                    allActiveIndexes.insert(allTrails[i][j]);
                }
            }
        }
        if(allTrails.empty()){
            return true;
        }
        if(allActiveIndexes.size() == cartesian->size() - cartesian->nOutputs) {
            return true;
        }
        uint rowNumber = cartesian->getRowNumber(whoIsMutated);
        uint newConnection = cartesian->randomConnectionGenerator(rowNumber);
        std::set<uint> allPossibleConnections = cartesian->allPossibleConnection(rowNumber);
        std::set<uint> tempSet;
        for(std::set<uint>::iterator it = allPossibleConnections.begin(); it != allPossibleConnections.end(); it++) {
            if(allActiveIndexes.count(*it) > 0) {
                continue;
            }
            else {
                tempSet.insert(*it);
            }
        }
        allPossibleConnections = tempSet;
        if(allPossibleConnections.empty()) {
            return true;
        }
        while(allActiveIndexes.count(newConnection)) {
            newConnection = cartesian->randomConnectionGenerator(rowNumber);
        }
        for(uint i = 0; i < activeTrail.size(); i++) {
            CartesianGene& cg = cartesian->operator[](activeTrail[i]);
            if(activeTrail[i] + cartesian->nInputs > newConnection) {
                for(uint j = 0; j < cg.inputConnections.size(); j++) {
                    if(cg.inputConnections[j] == whoIsMutated) {
                        cg.inputConnections[j] = newConnection;
                    }
                }
            }
        }
        return true;
    }
}