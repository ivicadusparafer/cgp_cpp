#include <set>
#include "MutConnectionSilent.h"
#include "Cartesian_genotype.h"
#include "FunctionSet.h"
namespace cartesian{
    void MutateConnectionSilent::registerParameters(StateP state)
    {
        myGenotype_->registerParameter(state, "mut.connectionsilent", (voidP) new double(0), ECF::DOUBLE);
    }

    bool MutateConnectionSilent::initialize(StateP state)
    {
        voidP sptr = myGenotype_->getParameterValue(state, "mut.connectionsilent");
        probability_ = *((double*)sptr.get());
        return true;
    }

    bool MutateConnectionSilent::mutate(GenotypeP gene)
    {
        Cartesian* cartesian = (Cartesian*) gene.get();
        const std::vector<FunctionP_basic>& vRef = cartesian->functionSet_->vActiveFunctions_;
        std::set<uint> allActiveIndexes;
        std::vector<std::vector<uint> > allTrails = cartesian->getActiveTrails();
        for(uint i = 0; i < allTrails.size(); i++) {
            for(uint j = 0; j < allTrails[i].size(); j++) {
                allActiveIndexes.insert(allTrails[i][j]);
            }
        }
        uint toBeMutated = cartesian->get_random_int(0, cartesian->size() - 1 - cartesian->nOutputs);
        if(allActiveIndexes.size() == cartesian->size() - cartesian->nOutputs) {
            return true;
        }
        while(allActiveIndexes.count(toBeMutated)) {
            toBeMutated = cartesian->get_random_int(0, cartesian->size() - 1 - cartesian->nOutputs);
        }
        uint oldFunctionId = cartesian->operator[](toBeMutated).value;
        uint rowNumber = cartesian->getRowNumber(toBeMutated);
        uint oldConnection = cartesian->get_random_int(0, vRef[oldFunctionId]->getNumOfArgs() - 1);
        uint newConnection = cartesian->randomConnectionGenerator(rowNumber);
        cartesian->operator[](toBeMutated).inputConnections[oldConnection] = newConnection;
        return true;
    }
}
