#ifndef ECF_CARTESIAN_FUNCTIONSET_H
#define ECF_CARTESIAN_FUNCTIONSET_H
#include "Function.h"
#include "Add.h"
#include "Sub.h"
#include "Mul.h"
#include "Div.h"
#include "Ln.h"
#include "Neg.h"
#include "Sin.h"
#include "Sqrt.h"
#include "Step.h"
#include "Cos.h"
#include "Max.h"
#include "Min.h"
#include "Sq_exp.h"
namespace cartesian{
    class FunctionSet{
    public:
        FunctionSet();
        ~FunctionSet(){};
        bool initialize(StateP state);
        bool addFunction(const std::string& name);

        std::map<std::string,FunctionP_basic> mAllFunctions_;
        std::vector<FunctionP_basic> vActiveFunctions_;
        std::map<std::string,FunctionP_basic> mActiveFunctions_;
        StateP state_;
    };
    typedef boost::shared_ptr<FunctionSet> FunctionSetP;
}
#endif //ECF_CARTESIAN_FUNCTIONSET_H
