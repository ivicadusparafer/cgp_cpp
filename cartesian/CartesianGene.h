#ifndef ECF_CARTESIAN_CARTESIANGENE_H
#define ECF_CARTESIAN_CARTESIANGENE_H
#include <vector>
#include <iostream>
namespace cartesian{
    typedef unsigned int uint;
    struct CartesianGene{
        CartesianGene(){};
        CartesianGene(uint value_, const std::vector<uint>& inputConnections_, bool isOutput_ = false) :
                value(value_), inputConnections(inputConnections_), isOutput(isOutput_) {};
        //value is ID of a function if CartesianGene is not an output node
        //If it is, value is index of a node (inside Cartesian) whose output
        //is forwarded to value.
        //Suggestion: Replace with union;
        uint value;
        std::vector<uint> inputConnections;
        bool isOutput;
        friend std::ostream& operator<<(std::ostream& out, const CartesianGene& cg) {
            out << cg.value << ' ';
            if(!cg.isOutput) {
                for(uint i = 0; i < cg.inputConnections.size(); i++) {
                    out << cg.inputConnections[i] << ' ';
                }
            }
            return out;
        }
    };
}
#endif //ECF_CARTESIAN_CARTESIANGENE_H
