#include <deque>
#include <ECF_base.h>
#include "Cartesian.h"
#include "Cartesian_genotype.h"
namespace cartesian{

    Cartesian::Cartesian()
    {
        name_ = "Cartesian";
    }


    Cartesian* Cartesian::copy()
    {
        Cartesian *newObject = new Cartesian(*this);

        return newObject;

    }
    std::vector<CrossoverOpP> Cartesian::getCrossoverOp()
    {
        std::vector<CrossoverOpP> crossoverOperators;
        crossoverOperators.push_back((CrossoverOpP) (new CartesianCrxUniform));
        crossoverOperators.push_back((CrossoverOpP) (new CartesianCrxHalfUniform));
        crossoverOperators.push_back((CrossoverOpP) (new CartesianCrxOnePoint));
        return crossoverOperators;
    }

    std::vector<MutationOpP> Cartesian::getMutationOp()
    {
        std::vector<MutationOpP> mutationOperators;
        mutationOperators.push_back((MutationOpP) (new MutateFunction));
        mutationOperators.push_back((MutationOpP) (new MutateFunctionSilent));
        mutationOperators.push_back((MutationOpP) (new MutateFunctionSwap));
        mutationOperators.push_back((MutationOpP) (new MutateConnection));
        mutationOperators.push_back((MutationOpP) (new MutateConnectionSilent));
        mutationOperators.push_back((MutationOpP) (new MutateIntoSilent));
        return mutationOperators;
    }

    void Cartesian::registerParameters(StateP state)
    {
        registerParameter(state, "numoutputs", (voidP) (new uint(1)), ECF::UINT, "number of functional outputs (default: 1)");
        registerParameter(state, "numrows", (voidP) (new uint(1)), ECF::UINT, "number of rows (default: 1)");
        registerParameter(state, "numcols", (voidP) (new uint(100)), ECF::UINT, "number of columns (default: 100)");
        registerParameter(state, "levelsback", (voidP) (new uint(1)), ECF::UINT, "number of previous columns to be used as possible inputs (default: 1)");
        registerParameter(state, "numvariables", (voidP) (new uint(1)), ECF::UINT, "number of input variables (default: 1)");
        registerParameter(state, "constantset", (voidP) (new std::string), ECF::STRING, "set of functions to use (default: none)");
        registerParameter(state, "functionset", (voidP) (new std::string), ECF::STRING, "set of input constants (default: none)");
    }


    bool Cartesian::initialize(StateP state)
    {
        state_ = state;
        std::stringstream ss;
        std::string names,name;
        voidP sptr;

        // create and initialize the function set
        functionSet_ = static_cast<FunctionSetP> (new FunctionSet);
        functionSet_->initialize(state_);


        uint number;
        //Simple parameters
        sptr = getParameterValue(state, "numvariables");
        number = *((uint*) sptr.get());
        if(number <= 0) {
            std::cerr << "Genotype initialization error: Number of variables is lesser than 1 or can not be parsed into a number.\n";
            return false;
        }
        nVariables = number;

        sptr = getParameterValue(state,"numoutputs");
        number = *((uint*) sptr.get());
        if(number <= 0) {
            std::cerr << "Genotype initialization error: Number of outputs is lesser than 1 or can not be parsed into a number.\n";
            return false;
        }
        nOutputs = number;

        sptr = getParameterValue(state,"numrows");
        number = *((uint*) sptr.get());
        if(number <= 0) {
            std::cerr << "Genotype initialization error: Number of rows is lesser than 1 or can not be parsed into a number.\n";
            return false;
        }
        nRows = number;

        sptr = getParameterValue(state,"numcols");
        number = *((uint*) sptr.get());
        if(number <= 0) {
            std::cerr << "Genotype initialization error: Number of columns is lesser than 1 or can not be parsed into a number.\n";
            return false;
        }
        nCols = number;

        sptr = getParameterValue(state,"levelsback");
        number = *((uint*) sptr.get());
        if(number <= 0) {
            std::cerr << "Genotype initialization error: Number of columns is lesser than 1 or can not be parsed into a number.\n";
            return false;
        }
        nLevelsBack = number;
        if(nLevelsBack > nCols) {
            nLevelsBack = nCols;
        }

        sptr = getParameterValue(state,"constantset");
        names = *((std::string*) sptr.get());
        number = 0;
        ss.str("");
        ss.clear();
        ss << names;
        while(ss >> name) {
            ++number;
        }
        nConstants = number;
        nInputs = nConstants + nVariables;

        //Functionset parametri su malo složeniji
        sptr = getParameterValue(state, "functionset");
        names = *((std::string*) sptr.get());
        ss.str("");
        ss.clear();
        ss << names;
        name="";

        while(ss >> name) {
            functionSet_->addFunction(name);
            nFunctions++;
        }

        std::map<std::string, FunctionP_basic>::iterator it;
        for (it = functionSet_->mActiveFunctions_.begin(); it != functionSet_->mActiveFunctions_.end(); it++) {
            uint nArgs = it->second->getNumOfArgs();
            if (nArgs > maxArity)
            {
                maxArity = nArgs;
            }
        }
        buildRandomGenome();
        return true;
    }

    void Cartesian::read(XMLNode &xCart)
    {
        std::string s = xCart.getText(0);
        std::istringstream ss(s);
        std::string token;
        uint i = 0;
        bool flag = false;
        uint functionId = 0;
        std::vector<uint> operands;
        uint counter = 0;
        while (getline(ss, token, ' '))
        {
            if(!flag) {
                functionId = str2uint(token);
                flag = true;
                counter = functionSet_->vActiveFunctions_[functionId]->getNumOfArgs();
            }
            else{
                operands.push_back(str2uint(token));
                counter--;
                if(counter == 0) {
                    flag = false;
                    this->at(i++) = CartesianGene(functionId,operands);
                }
            }
        }
        for(uint j = this->size() - 1; j > this->size() - 1 - nOutputs; j--) {
            this->operator[](j).isOutput = true;
        }
    }

    void Cartesian::write(XMLNode &xCart)
    {
        xCart = XMLNode::createXMLTopNode("Cartesian");
        std::stringstream sValue;
        sValue << this->size();
        xCart.addAttribute("size", sValue.str().c_str());

        sValue.str("");

        // write genome to sValue
        std::vector<CartesianGene>& genome = *this;
        for(uint i = 0; i < genome.size(); i++) {
            sValue << genome[i];
        }
        xCart.addText(sValue.str().c_str());
    }

    uint Cartesian::getGenomeSize()
    {
        return this->size();
    }

    uint Cartesian::getRowNumber(uint index)
    {
        return index/nCols;
    }

/**
 * CGP layout: On top is 1st row, on bottom is nth row.
 * Depending on which row we wish to connect we need its number to check valid connections(only rows
 * before it, depending on levelsback).
 *
 * ROWS AND COLUMNS ARE 0 indexed!!!
 *
 * */
    uint Cartesian::randomConnectionGenerator(uint rowNumber)
    {
        uint minimum = nInputs + nCols * nLevelsBack;

        //Index of the first element of a row
        uint firstElementOfARow = nInputs + rowNumber*nCols;
        if(minimum <= firstElementOfARow) {
            minimum = firstElementOfARow - nLevelsBack*nCols;
        }
        else {
            minimum = 0;
        }
        return state_->getRandomizer()->getRandomInteger(minimum, firstElementOfARow -1);
    }

    std::set<uint> Cartesian::allPossibleConnection(uint rowNumber)
    {
        uint minimum = nInputs + nCols * nLevelsBack;
        //Index of the first element of a row
        uint firstElementOfARow = nInputs + rowNumber*nCols;
        if(minimum <= firstElementOfARow) {
            minimum = firstElementOfARow - nLevelsBack*nCols;
        }
        else {
            minimum = 0;
        }
        std::set<uint> all;
        for(uint i = minimum; i < firstElementOfARow; i++) {
            all.insert(i);
        }
        return all;
    }

/**
 * output_number is 0 indexed.
 * Returned vector contains indexes of Cartesian object. All CartesianGenes
 * contribute in calculation of an output numbered output_number.
 * */
    std::vector<uint> Cartesian::getActiveTrail(uint output_number)
    {
        std::vector<uint> trail;
        std::deque<uint> working_deque;
        uint start_index = this->size() - nOutputs + output_number;
        uint start_value = this->operator[](start_index).value;
        working_deque.push_back(start_value);
        while(!working_deque.empty()) {
            uint working_index = working_deque.front();
            working_deque.pop_front();
            if(working_index >= nInputs) {
                trail.push_back(working_index - nInputs);
                const CartesianGene& gene = this->at(working_index - nInputs);
                if(gene.isOutput) {
                    std::cout << "Something went wrong. Trail is considering an output index.\n";
                }
                for(uint i = 0; i < gene.inputConnections.size(); i++) {
                    working_deque.push_back(gene.inputConnections[i]);
                }
            }
        }
        return trail;
    }

    std::vector<std::vector<uint> > Cartesian::getActiveTrails()
    {
        std::vector<std::vector<uint> > allTrails;
        for(uint i = 0; i < nOutputs; i++) {
            allTrails.push_back(getActiveTrail(i));
        }
        return allTrails;
    }

    void Cartesian::buildRandomGenome()
    {
        const std::vector<FunctionP_basic>& vRef = functionSet_->vActiveFunctions_;
        for(uint i = 0; i < nRows; i++) {
            for(uint j = 0; j < nCols; j++) {
                uint functionID = get_random_int(0, functionSet_->vActiveFunctions_.size() - 1);
                uint noOfOperands = vRef[functionID]->getNumOfArgs();
                std::vector<uint> connections;
                for(uint k = 0; k < noOfOperands; k++) {
                    connections.push_back(randomConnectionGenerator(i));
                }
                this->push_back(CartesianGene(functionID,connections));
            }
        }
        for(uint i = 0; i < nOutputs; i++) {
            this->push_back(CartesianGene(randomConnectionGenerator(nRows),std::vector<uint>(),true));
        }
    }


    int Cartesian::get_random_int(int from, int to) {
        return this->state_->getRandomizer()->getRandomInteger(from,to);
    }


    double Cartesian::get_random_double(int from, int to) {
        return  rand() % (to - from + 1) + from;
    }

    void Cartesian::evaluate(const std::vector<double> &inputData, std::vector<double> &results)
    {
        std::vector<double> working_vector(inputData);
        std::vector<double> operands;
        const std::vector<FunctionP_basic>& vRef = functionSet_->vActiveFunctions_;
        double result = 0;

        for(uint i = 0; i < this->size() - nOutputs; i++) {
            const CartesianGene& gene = this->operator[](i);
            for(uint j = 0; j < gene.inputConnections.size(); j++) {
                operands.push_back(working_vector[gene.inputConnections[j]]);
            }
            vRef[gene.value]->evaluate(operands,result);
            working_vector.push_back(result);
            operands.clear();
            result = 0;
        }
        for(uint i = this->size() - nOutputs; i < this->size(); i++) {
            results.push_back(working_vector[this->operator[](i).value]);
        }
    }

    double Cartesian::static_random_double(int from, int to) {
        return rand() % (to - from + 1) + from;
    }
}