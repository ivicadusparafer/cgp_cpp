#ifndef ECF_CARTESIAN_NEG_H
#define ECF_CARTESIAN_NEG_H
#include "Function.h"
namespace cartesian{
    template <typename Container, typename Result>
    class Neg : public Function<Container,Result> {
    public:
        Neg();
        ~Neg(){};
        void evaluate(Container& container, Result& result);
    };

    template <typename Container, typename Result>
    Neg<Container,Result>::Neg()
    {
        this->name_ = "neg";
        this->numOfArgs_ = 1;
    }

    template <typename Container, typename Result>
    void Neg<Container,Result>::evaluate(Container& container, Result& result)
    {
        typename Container::iterator it = container.begin();
        result = -1 * (*it);
    }
}
#endif //ECF_CARTESIAN_NEG_H
