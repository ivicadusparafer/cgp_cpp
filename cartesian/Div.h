#ifndef ECF_CARTESIAN_DIV_H
#define ECF_CARTESIAN_DIV_H
#include "Function.h"
namespace cartesian{
    const double DIVISION_THRESHOLD = 10e-7;
    template <typename Container, typename Result>
    class Div : public Function<Container,Result> {
    public:
        Div();
        ~Div(){};
        void evaluate(Container& container, Result& result);
    };

    template <typename Container, typename Result>
    Div<Container,Result>::Div()
    {
        this->name_ = "/";
        this->numOfArgs_ = 2;
    }

    template <typename Container, typename Result>
    void Div<Container,Result>::evaluate(Container& container, Result& result)
    {
        result = *(container.begin());
        for(typename Container::iterator it = container.begin() + 1; it != container.begin() + this->numOfArgs_; it++) {
            if(*it <= DIVISION_THRESHOLD) {
                continue;
            }
            result /= *it;
        }
    }
}
#endif //ECF_CARTESIAN_DIV_H
