#ifndef ECF_CARTESIAN_MUTFUNCTIONSILENT_H
#define ECF_CARTESIAN_MUTFUNCTIONSILENT_H
#include <ECF_base.h>
namespace cartesian{
    class MutateFunctionSilent : public MutationOp
    {
    public:
        void registerParameters(StateP state);
        bool initialize(StateP state);
        bool mutate(GenotypeP gene);
    };
    typedef boost::shared_ptr<MutateFunctionSilent> MutateFunctionSilentP;
}
#endif //ECF_CARTESIAN_MUTFUNCTIONSILENT_H
