#ifndef ECF_CARTESIAN_MIN_H
#define ECF_CARTESIAN_MIN_H
#include "Function.h"
namespace cartesian{
    template <typename Container, typename Result>
    class Min : public Function<Container,Result> {
    public:
        Min();
        ~Min(){};
        void evaluate(Container& container, Result& result);
    };

    template <typename Container, typename Result>
    Min<Container,Result>::Min()
    {
        this->name_ = "min";
        this->numOfArgs_ = 2;
    }

    template <typename Container, typename Result>
    void Min<Container,Result>::evaluate(Container& container, Result& result)
    {
        result = *container.begin();
        for(typename Container::iterator it = container.begin(); it != container.begin() + this->numOfArgs_; it++) {
            result = (*it) > result ? (result) : (*it);
        }
    }
}
#endif //ECF_CARTESIAN_MIN_H
