#include <set>
#include "MutFunctionSilent.h"
#include "Cartesian_genotype.h"
#include "FunctionSet.h"
namespace cartesian{

    void MutateFunctionSilent::registerParameters(StateP state)
    {
        myGenotype_->registerParameter(state, "mut.funcsilent", (voidP) new double(0), ECF::DOUBLE);
    }

    bool MutateFunctionSilent::initialize(StateP state)
    {
        voidP sptr = myGenotype_->getParameterValue(state, "mut.funcsilent");
        probability_ = *((double*)sptr.get());
        return true;
    }

    bool MutateFunctionSilent::mutate(GenotypeP gene)
    {
        Cartesian* cartesian = (Cartesian*) gene.get();
        const std::vector<FunctionP_basic>& vRef = cartesian->functionSet_->vActiveFunctions_;
        std::set<uint> allActiveIndexes;
        std::vector<std::vector<uint> >  allTrails = cartesian->getActiveTrails();
        for(uint i = 0; i < allTrails.size(); i++) {
            for(uint j = 0; j < allTrails[i].size(); j++) {
                allActiveIndexes.insert(allTrails[i][j]);
            }
        }
        uint toBeMutated = cartesian->get_random_int(0, cartesian->size() - 1 -cartesian->nOutputs);
        if(allActiveIndexes.size() == cartesian->size() - cartesian->nOutputs) {
            return true;
        }
        while(allActiveIndexes.count(toBeMutated)) {
            toBeMutated = cartesian->get_random_int(0, cartesian->size() - 1 -cartesian->nOutputs);
        }
        uint oldFunctionId = cartesian->operator[](toBeMutated).value;
        uint oldNumOfArgs = vRef[oldFunctionId]->getNumOfArgs();
        uint newFunctionId = cartesian->get_random_int(0, vRef.size() - 1);
        while(newFunctionId == oldFunctionId) {
            newFunctionId = cartesian->get_random_int(0, vRef.size() - 1);
        }
        uint newNumOfArgs = vRef[newFunctionId]->getNumOfArgs();
        //Assign a new function to the node.
        cartesian->operator[](toBeMutated).value = newFunctionId;

        //old and new function may not have the same amount of operators.
        if(oldNumOfArgs < newNumOfArgs){
            uint rowNumber = cartesian->getRowNumber(toBeMutated);
            while(oldNumOfArgs < newNumOfArgs) {
                uint connection = cartesian->randomConnectionGenerator(rowNumber);
                cartesian->operator[](toBeMutated).inputConnections.push_back(connection);
                oldNumOfArgs++;
            }
        }
        else if(oldNumOfArgs > newNumOfArgs) {
            while(oldNumOfArgs > newNumOfArgs) {
                uint connection = cartesian->get_random_int(0, oldNumOfArgs - 1);
                cartesian->operator[](toBeMutated).inputConnections.erase(cartesian->operator[](toBeMutated).inputConnections.begin() + connection);
                oldNumOfArgs--;
            }
        }
        return true;
    }
}