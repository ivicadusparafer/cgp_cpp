#ifndef ECF_CARTESIAN_CARTESIANCRXUNIFORM_H
#define ECF_CARTESIAN_CARTESIANCRXUNIFORM_H
#include <ECF_base.h>
namespace cartesian {
    class CartesianCrxUniform: public CrossoverOp
    {
    public:
        void registerParameters(StateP state);
        bool initialize(StateP state);
        bool mate(GenotypeP gen1, GenotypeP gen2, GenotypeP child);
    };
    typedef boost::shared_ptr<CartesianCrxUniform> CartesianCrxUniformP;
}
#endif //ECF_CARTESIAN_CARTESIANCRXUNIFORM_H
