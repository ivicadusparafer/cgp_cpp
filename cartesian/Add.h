#ifndef ECF_CARTESIAN_ADD_H
#define ECF_CARTESIAN_ADD_H
#include "Function.h"
namespace cartesian{
    template <typename Container, typename Result>
    class Add : public Function<Container,Result> {
    public:
        Add();
        ~Add() {};
        void evaluate(Container& container, Result& result);
    };

    template <typename Container, typename Result>
    Add<Container,Result>::Add()
    {
        this->name_ = "+";
        this->numOfArgs_ = 2;
    }

    template <typename Container, typename Result>
    void Add<Container,Result>::evaluate(Container& container, Result& result)
    {
        result = 0;
        for(typename Container::iterator it = container.begin(); it != container.end() + this->numOfArgs_; it++) {
            result += *it;
        }
    }
}
#endif //ECF_CARTESIAN_ADD_H
