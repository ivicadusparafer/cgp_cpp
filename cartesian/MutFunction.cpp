#include "MutFunction.h"
#include "Cartesian_genotype.h"
#include "FunctionSet.h"
namespace cartesian{

    void MutateFunction::registerParameters(StateP state)
    {
        myGenotype_->registerParameter(state, "mut.func", (voidP) new double(0), ECF::DOUBLE);
    }

    bool MutateFunction::initialize(StateP state)
    {
        voidP sptr = myGenotype_->getParameterValue(state, "mut.func");
        probability_ = *((double*)sptr.get());
        return true;
    }

    bool MutateFunction::mutate(GenotypeP gene)
    {
        Cartesian* cartesian = (Cartesian*) gene.get();
        const std::vector<FunctionP_basic>& vRef = cartesian->functionSet_->vActiveFunctions_;

        //First a random output is chosen. A function that affects this output will be mutated.
        uint whichOutput = cartesian->get_random_int(0, cartesian->nOutputs - 1);
        std::vector<uint> trail = cartesian->getActiveTrail(whichOutput);
        uint whoIsMutated = trail[cartesian->get_random_int(0, trail.size() - 1)];
        uint oldFunctionId = cartesian->operator[](whoIsMutated).value;
        uint oldNumOfArgs = vRef[oldFunctionId]->getNumOfArgs();

        uint newFunctionId = cartesian->get_random_int(0, vRef.size() - 1);
        while(newFunctionId == oldFunctionId) {
            newFunctionId = cartesian->get_random_int(0, vRef.size() - 1);
        }
        uint newNumOfArgs = vRef[newFunctionId]->getNumOfArgs();

        //Assign a new function to the node.
        cartesian->operator[](whoIsMutated).value = newFunctionId;

        //old and new function may not have the same amount of operators.
        if(oldNumOfArgs < newNumOfArgs){
            uint rowNumber = cartesian->getRowNumber(whoIsMutated);
            while(oldNumOfArgs < newNumOfArgs) {
                uint connection = cartesian->randomConnectionGenerator(rowNumber);
                cartesian->operator[](whoIsMutated).inputConnections.push_back(connection);
                oldNumOfArgs++;
            }
        }
        else if(oldNumOfArgs > newNumOfArgs) {
            while(oldNumOfArgs > newNumOfArgs) {
                uint connection = cartesian->get_random_int(0, oldNumOfArgs - 1);
                cartesian->operator[](whoIsMutated).inputConnections.erase(cartesian->operator[](whoIsMutated).inputConnections.begin() + connection);
                oldNumOfArgs--;
            }
        }
        return true;
    }
}