#ifndef ECF_CARTESIAN_LN_H
#define ECF_CARTESIAN_LN_H
#include "Function.h"
namespace cartesian{
    template <typename Container, typename Result>
    class Nlog : public Function<Container,Result> {
    public:
        Nlog();
        ~Nlog(){};
        void evaluate(Container& container, Result& result);
    };

    template <typename Container, typename Result>
    Nlog<Container,Result>::Nlog()
    {
        this->name_ = "ln";
        this->numOfArgs_ = 1;
    }

    template <typename Container, typename Result>
    void Nlog<Container,Result>::evaluate(Container& container, Result& result)
    {
        typename Container::iterator it = container.begin();
        if(std::isnan(*it) || std::isinf(*it) || *it == 0) {
            result = 1.0;
        }
        else if(*it < 0.0) {
            result = std::log(std::abs(*it));
        }
        else {
            result = std::log(*it);
        }
    }
}
#endif //ECF_CARTESIAN_LN_H
