#ifndef ECF_CARTESIAN_MAX_H
#define ECF_CARTESIAN_MAX_H
#include "Function.h"
namespace cartesian{
    template <typename Container, typename Result>
    class Max : public Function<Container,Result> {
    public:
        Max();
        ~Max(){};
        void evaluate(Container& container, Result& result);
    };

    template <typename Container, typename Result>
    Max<Container,Result>::Max()
    {
        this->name_ = "max";
        this->numOfArgs_ = 2;
    }

    template <typename Container, typename Result>
    void Max<Container,Result>::evaluate(Container& container, Result& result)
    {

        for(typename Container::iterator it = container.begin(); it != container.end() + this->numOfArgs_; it++) {
            result = (*it) > result ? (*it) : result;
        }
    }
}
#endif //ECF_CARTESIAN_MAX_H
