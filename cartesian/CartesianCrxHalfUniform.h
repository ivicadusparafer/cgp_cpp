#ifndef ECF_CARTESIAN_CARTESIANCRXHALFUNIFORM_H
#define ECF_CARTESIAN_CARTESIANCRXHALFUNIFORM_H
#include <ECF_base.h>
namespace cartesian {
    class CartesianCrxHalfUniform: public CrossoverOp
    {
    public:
        void registerParameters(StateP state);
        bool initialize(StateP state) ;
        bool mate(GenotypeP gen1, GenotypeP gen2, GenotypeP child);
    };
    typedef boost::shared_ptr<CartesianCrxHalfUniform> CartesianCrxHalfUniformP;
}
#endif //ECF_CARTESIAN_CARTESIANCRXHALFUNIFORM_H
