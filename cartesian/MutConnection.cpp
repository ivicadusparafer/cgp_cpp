#include "MutConnection.h"
#include "Cartesian_genotype.h"
#include "FunctionSet.h"
namespace cartesian{
    void MutateConnection::registerParameters(StateP state)
    {
        myGenotype_->registerParameter(state, "mut.connection", (voidP) new double(0), ECF::DOUBLE);
    }

    bool MutateConnection::initialize(StateP state)
    {
        voidP sptr = myGenotype_->getParameterValue(state, "mut.connection");
        probability_ = *((double*)sptr.get());
        return true;
    }

    bool MutateConnection::mutate(GenotypeP gene)
    {
        Cartesian* cartesian = (Cartesian*) gene.get();
        const std::vector<FunctionP_basic>& vRef = cartesian->functionSet_->vActiveFunctions_;
        //Connection of which output will be mutated?
        uint whichOutput = cartesian->get_random_int(0, cartesian->nOutputs - 1);
        std::vector<uint> trail = cartesian->getActiveTrail(whichOutput);
        uint whoIsMutated = trail[cartesian->get_random_int(0, trail.size() - 1)];
        uint rowNumber = cartesian->getRowNumber(whoIsMutated);
        uint whichConnection = cartesian->get_random_int(0, vRef[cartesian->operator[](whoIsMutated).value]->getNumOfArgs() - 1);
        uint newConnection = cartesian->randomConnectionGenerator(rowNumber);
        cartesian->operator[](whoIsMutated).inputConnections[whichConnection] = newConnection;
        return true;
    }
}