#ifndef ECF_CARTESIAN_MUTFUNCTION_H
#define ECF_CARTESIAN_MUTFUNCTION_H
#include <ECF_base.h>
namespace cartesian{
    class MutateFunction : public MutationOp
    {
    public:
        void registerParameters(StateP state);
        bool initialize(StateP state);
        bool mutate(GenotypeP gene);
    };
    typedef boost::shared_ptr<MutateFunction> MutateFunctionNonSilentP ;
}
#endif //ECF_CARTESIAN_MUTFUNCTION_H
