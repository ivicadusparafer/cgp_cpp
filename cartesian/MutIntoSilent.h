#ifndef ECF_CARTESIAN_MUTINTOSILENT_H
#define ECF_CARTESIAN_MUTINTOSILENT_H
#include <ECF_base.h>
namespace cartesian{
    class MutateIntoSilent : public MutationOp
    {
    public:
        void registerParameters(StateP state);
        bool initialize(StateP state);
        bool mutate(GenotypeP gene);
    };
    typedef boost::shared_ptr<MutateIntoSilent> MutateIntoSilentP;
}
#endif //ECF_CARTESIAN_MUTINTOSILENT_H
