#ifndef ECF_CARTESIAN_MUTFUNCTIONSWAP_H
#define ECF_CARTESIAN_MUTFUNCTIONSWAP_H
#include <ECF_base.h>
namespace cartesian{
    class MutateFunctionSwap : public MutationOp
    {
    public:
        void registerParameters(StateP state);
        bool initialize(StateP state);
        bool mutate(GenotypeP gene);
    };
    typedef boost::shared_ptr<MutateFunctionSwap> MutateFunctionSwapP;
}
#endif //ECF_CARTESIAN_MUTFUNCTIONSWAP_H
