#ifndef ECF_CARTESIAN_COS_H
#define ECF_CARTESIAN_COS_H
#include "Function.h"
namespace cartesian{
    template <typename Container, typename Result>
    class Cos : public Function<Container,Result> {
    public:
        Cos();
        ~Cos(){};
        void evaluate(Container& container, Result& result);
    };

    template <typename Container, typename Result>
    Cos<Container,Result>::Cos()
    {
        this->name_ = "cos";
        this->numOfArgs_ = 1;
    }

    template <typename Container, typename Result>
    void Cos<Container,Result>::evaluate(Container& container, Result& result)
    {
        typename Container::iterator it = container.begin();
        result = std::cos(*it);
    }
}
#endif //ECF_CARTESIAN_COS_H
