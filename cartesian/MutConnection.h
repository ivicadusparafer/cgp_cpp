#ifndef ECF_CARTESIAN_MUTCONNECTION_H
#define ECF_CARTESIAN_MUTCONNECTION_H
#include <ECF_base.h>
namespace cartesian{
    class MutateConnection : public MutationOp
    {
    public:
        void registerParameters(StateP state);
        bool initialize(StateP state);
        bool mutate(GenotypeP gene);
    };
    typedef boost::shared_ptr<MutateConnection> MutateConnectionP;
}
#endif //ECF_CARTESIAN_MUTCONNECTION_H
