#ifndef ECF_CARTESIAN_SQ_EXP_H
#define ECF_CARTESIAN_SQ_EXP_H
#include "Function.h"
namespace cartesian{
    template <typename Container, typename Result>
    class Sq_exp : public Function<Container,Result> {
    public:
        Sq_exp();
        ~Sq_exp(){};
        void evaluate(Container& container, Result& result);
    };

    template <typename Container, typename Result>
    Sq_exp<Container,Result>::Sq_exp()
    {
        this->name_ = "sq_exp";
        this->numOfArgs_ = 1;
    }

    template <typename Container, typename Result>
    void Sq_exp<Container,Result>::evaluate(Container& container, Result& result)
    {
        typename Container::iterator it = container.begin();
        result = std::pow(*it,2);
    }
}
#endif //ECF_CARTESIAN_SQ_EXP_H
