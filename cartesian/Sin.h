#ifndef ECF_CARTESIAN_SIN_H
#define ECF_CARTESIAN_SIN_H
#include "Function.h"
namespace cartesian{
    template <typename Container, typename Result>
    class Sin : public Function<Container,Result> {
    public:
        Sin();
        ~Sin(){};
        void evaluate(Container& container, Result& result);
    };

    template <typename Container, typename Result>
    Sin<Container,Result>::Sin()
    {
        this->name_ = "sin";
        this->numOfArgs_ = 1;
    }

    template <typename Container, typename Result>
    void Sin<Container,Result>::evaluate(Container& container, Result& result)
    {
        typename Container::iterator it = container.begin();
        result = std::sin(*it);
    }
}
#endif //ECF_CARTESIAN_SIN_H
