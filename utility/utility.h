#ifndef CARTESIAN_UTILITY_H
#define CARTESIAN_UTILITY_H
#include <vector>
namespace utility{
    typedef unsigned int uint;
    uint vectorArgmax(const std::vector<double>& v);
    std::vector<std::vector<int> > initializeEmptyConfusionMatrix(const std::vector<int>& classes);
}
#endif //CARTESIAN_UTILITY_H
