#ifndef ECF_CARTESIAN_EXPRESSION_EVALUATION_H
#define ECF_CARTESIAN_EXPRESSION_EVALUATION_H
#include <string>
#include <map>
#include <set>
#include <stack>
#include <iostream>
#include <cmath>
#include <vector>
#include <boost/algorithm/string.hpp>

namespace utility{
    static std::string init[] =  {"+","-","*","/","^","sqrt","sin","max","min","cos","ln"};
    static std::set<std::string> existingOperators(init, init + sizeof(init)/sizeof(init[0]) );
    double evaluateExpression(const std::map<std::string,double>& variables, std::string expression);
}
#endif //ECF_CARTESIAN_EXPRESSION_EVALUATION_H
