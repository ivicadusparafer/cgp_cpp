#include <ECF.h>
#include "cartesian/Cartesian.h"
#include "SymbRegEvalOp.h"
#include "MultipleClassEvalOp.h"
int main(int argc, char** argv) {
    StateP state (new State);
    CartesianP cgp (new cartesian::Cartesian);
    state->addGenotype(cgp);
    state->setEvalOp(new SymbRegEvalOp);
    state->initialize(argc,argv);

    state->run();
    return 0;
}
