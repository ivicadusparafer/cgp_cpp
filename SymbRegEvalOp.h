#ifndef ECF_CARTESIAN_SYMBREGEVALOP_H
#define ECF_CARTESIAN_SYMBREGEVALOP_H
#include <ECF.h>
#include <EvaluateOp.h>
#include <ECF_macro.h>
#include "utility/expression_evaluation.h"
#include "utility/measures.h"
#include "utility/fileparser.h"
#include <iostream>
#include <fstream>
#include <istream>
#include <sstream>
class SymbRegEvalOp : public EvaluateOp
{
private:
    std::vector<std::vector<double> > inputs;
    std::vector<double> output;
    std::vector<double> kappaOutput;
    std::vector<double> thetaOutput;
    std::string equation;
    double softTargetBeta;
    double softTargetGamma;
    bool softTarget;
    void addConstants(uint nConstants);
public:
    SymbRegEvalOp(){softTarget = false;};
    void registerParameters(StateP stateP);
    bool initialize(StateP stateP);
    FitnessP evaluate(IndividualP individual);
};
#endif //ECF_CARTESIAN_SYMBREGEVALOP_H
