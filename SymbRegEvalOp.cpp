#include "SymbRegEvalOp.h"
#include "./cartesian/Cartesian.h"

void SymbRegEvalOp::addConstants(uint nConstants)
{
    for(uint i = 0; i < inputs.size(); i++) {
        for(uint j = 0; j < nConstants; j++) {
            inputs[i].push_back(cartesian::Cartesian::static_random_double(-10.0,10.0));
        }
    }
}


void SymbRegEvalOp::registerParameters(StateP stateP)
{
    stateP->getRegistry()->registerEntry("training.infile", (voidP) new (std::string), ECF::STRING);
    stateP->getRegistry()->registerEntry("expression.infile", (voidP) new(std::string), ECF::STRING);
    stateP->getRegistry()->registerEntry("softtarget", (voidP) new(std::string), ECF::STRING);
    stateP->getRegistry()->registerEntry("softtarget.gamma", (voidP) new(std::string), ECF::STRING);
    stateP->getRegistry()->registerEntry("softtarget.beta", (voidP) new(std::string), ECF::STRING);
}

bool SymbRegEvalOp::initialize(StateP stateP)
{
    if(!stateP->getRegistry()->isModified("expression.infile")) {
        ECF_LOG_ERROR(stateP, "Expression is not registered as a parameter. Register a path to a expression in parameters.txt file.");
        return false;
    }
    voidP vp1 = stateP->getRegistry()->getEntry("expression.infile");
    std::string path1 = *((std::string*)vp1.get());
    std::ifstream in_file(path1.c_str());
    if(!in_file) {
        std::cerr << "Parser could not load a expression file: " << path1 << '\n';
        exit(-1);
    }
    std::string line;
    getline(in_file,line);
    in_file.close();
    this->equation = line;
    if(!stateP->getRegistry()->isModified("training.infile")) {
        ECF_LOG_ERROR(stateP, "Error: no input file defined for arguments and function values.");
        return false;
    }
    vp1 = stateP->getRegistry()->getEntry("training.infile");
    path1 = *((std::string*)vp1.get());
    std::ifstream in_file2(path1.c_str());
    if(!in_file2) {
        std::cerr << "Could not load a file which defines arguments / function values for given function.\n";
        exit(-1);
    }
    for(std::string line2; getline(in_file2,line2);) {
        std::pair<std::vector<double>,double> result = utility::parseArgumentsAndFunctionValues(line2);
        this->inputs.push_back(result.first);
        this->output.push_back(result.second);
    }
    if(stateP->getRegistry()->isModified("softtarget")) {
        voidP vpS = stateP->getRegistry()->getEntry("softtarget");
        std::string sa = *((std::string*)vpS.get());
        if(sa == "1" || sa == "yes" || sa == "Yes" || sa == "true") {
            softTarget = true;
            ECF_LOG(stateP,0,"Soft target regularization is used.");
            if(!stateP->getRegistry()->isModified("softtarget.beta")) {
                ECF_LOG_ERROR(stateP,"Could not find beta factor for softtarget. Define softtarget.beta entry in registry.\n");
                exit(-1);
            }
            else {
                voidP vpSBeta = stateP->getRegistry()->getEntry("softtarget.beta");
                std::string vbeta = *((std::string*)vpSBeta.get());
                try{
                    softTargetBeta = str2dbl(vbeta);
                }catch(std::exception& ex) {
                    ECF_LOG_ERROR(stateP,"Soft target beta is not a number convertible to double.\n");
                    exit(-1);
                }
            }
            if(!stateP->getRegistry()->isModified("softtarget.gamma")) {
                ECF_LOG_ERROR(stateP,"Could not find gamma factor for softtarget. Define softtarget.gamma entry in registry.\n");
                exit(-1);
            }
            else {
                voidP vpSGamma = stateP->getRegistry()->getEntry("softtarget.gamma");
                std::string vgamma = *((std::string*)vpSGamma.get());
                try{
                    softTargetGamma = str2dbl(vgamma);
                }catch(std::exception& ex) {
                    ECF_LOG_ERROR(stateP, "Soft target gamma is not a number convertible to double.\n");
                    exit(-1);
                }
            }
        }
    }
    return true;
}

FitnessP SymbRegEvalOp::evaluate(IndividualP individual) {
    FitnessP fitness(new FitnessMin);
     cartesian::Cartesian* cartesian = (cartesian::Cartesian *) individual->getGenotype().get();
    //nInputs = nVariables + nConstants, constants are added to each example in a random fashion
    addConstants(cartesian->nConstants);
    std::vector<double> calculatedOutputs;
    for(uint i = 0; i < inputs.size(); i++) {
        std::vector<double> results;
        cartesian->evaluate(inputs[i], results);
        calculatedOutputs.push_back(results[0]);
    }
    double MSE;
    if (!softTarget) {
        MSE = utility::rootMeanSquareError(output, calculatedOutputs);
    } else {
        if (cartesian->state_->getGenerationNo() > 0) {
            if (kappaOutput.size() != calculatedOutputs.size()) {
                std::cout << "Theta output i calculated output nemaju jednake dimenzije.\n";
            }
            for (uint j = 0; j < kappaOutput.size(); j++) {
                kappaOutput[j] = softTargetBeta * kappaOutput[j] + (1 - softTargetBeta) * calculatedOutputs[j];
            }
            if (kappaOutput.size() != thetaOutput.size()) {
                std::cout << "Kappa output i Theta output nemaju jednake dimenzije.\n";
            }
            for (uint j = 0; j < thetaOutput.size(); j++) {
                thetaOutput[j] = softTargetGamma * kappaOutput[j] + (1 - softTargetGamma) * output[j];
            }
        } else {
            kappaOutput = calculatedOutputs;
            thetaOutput = output;
        }
        MSE = utility::rootMeanSquareError(thetaOutput, calculatedOutputs);
    }
    fitness->setValue(MSE);
    return fitness;
}