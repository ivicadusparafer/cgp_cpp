#include "MultipleClassEvalOp.h"
#include "./cartesian/Cartesian.h"
#include "utility/fileparser.h"
#include "./utility/measures.h"
#include "./utility/utility.h"
#include <boost/algorithm/string.hpp>

void MultipleClassEvalOp::registerParameters(StateP stateP)
{
    stateP->getRegistry()->registerEntry("training.infile", (voidP) new (std::string), ECF::STRING);
    stateP->getRegistry()->registerEntry("testing.infile", (voidP) new (std::string), ECF::STRING);
    stateP->getRegistry()->registerEntry("measure", (voidP) new (std::string), ECF::STRING);
    stateP->getRegistry()->registerEntry("hasID", (voidP) new (std::string), ECF::STRING);
}

bool MultipleClassEvalOp::initialize(StateP stateP)
{
    if(!stateP->getRegistry()->isModified("training.infile")) {
        ECF_LOG_ERROR(stateP, "Error: no input file defined for training");
        return false;
    }
    if(!stateP->getRegistry()->isModified("testing.infile")) {
        ECF_LOG_ERROR(stateP, "Error: no input file defined for testing");
        return false;
    }
    if(!stateP->getRegistry()->isModified("measure")) {
        ECF_LOG(stateP,0,"Default measure of accuracy is used.");
        measureUsed = "accuracy";
    }
    else {
        voidP vpM = stateP->getRegistry()->getEntry("measure");
        measureUsed = *((std::string*)vpM.get());
        boost::algorithm::to_lower(measureUsed);
    }
    if(!stateP->getRegistry()->isModified("hasID")) {
        leadsWithId = true;
    }
    //Load training from file.
    voidP vp1 = stateP->getRegistry()->getEntry("training.infile");
    std::string path1 = *((std::string*)vp1.get());
    std::vector<std::string> parsedTrainingCSV = utility::parseCSVFromFile(path1);
    PairAllFeaturesAllLabels ioTrainingPair = utility::parseStringIntoFeaturesAndLabels(parsedTrainingCSV, leadsWithId);
    trainingInput = ioTrainingPair.first;
    trainingOutputs = ioTrainingPair.second;

    //Load testing from file.
    voidP vp2 = stateP->getRegistry()->getEntry("testing.infile");
    std::string path2 = *((std::string*)vp2.get());
    std::vector<std::string> parsedTestCSV = utility::parseCSVFromFile(path2);
    PairAllFeaturesAllLabels ioTestPair = utility::parseStringIntoFeaturesAndLabels(parsedTestCSV, leadsWithId);
    testingInput = ioTestPair.first;
    testingOutputs = ioTestPair.second;

    std::set<uint> distinct;
    for(uint i = 0; i < trainingOutputs.size(); i++) {
        distinct.insert(trainingOutputs[i]);
    }
    numberOfDifferentClasses = distinct.size();

    if(stateP->getRegistry()->isModified("softtarget")) {
        voidP vpS = stateP->getRegistry()->getEntry("softtarget");
        std::string sa = *((std::string*)vpS.get());
        if(sa == "1" || sa == "yes" || sa == "Yes" || sa == "true") {
            softTarget = true;
            ECF_LOG(stateP,0,"Soft target regularization is used.");
            if(!stateP->getRegistry()->isModified("softtarget.beta")) {
                ECF_LOG_ERROR(stateP,"Could not find beta factor for softtarget. Define softtarget.beta entry in registry.\n");
                exit(-1);
            }
            else {
                voidP vpSBeta = stateP->getRegistry()->getEntry("softtarget.beta");
                std::string vbeta = *((std::string*)vpSBeta.get());
                try{
                    softTargetBeta = str2dbl(vbeta);
                }catch(std::exception& ex) {
                    ECF_LOG_ERROR(stateP,"Soft target beta is not a number convertible to double.\n");
                    exit(-1);
                }
            }
            if(!stateP->getRegistry()->isModified("softtarget.gamma")) {
                ECF_LOG_ERROR(stateP,"Could not find gamma factor for softtarget. Define softtarget.gamma entry in registry.\n");
                exit(-1);
            }
            else {
                voidP vpSGamma = stateP->getRegistry()->getEntry("softtarget.gamma");
                std::string vgamma = *((std::string*)vpSGamma.get());
                try{
                    softTargetGamma = str2dbl(vgamma);
                }catch(std::exception& ex) {
                    ECF_LOG_ERROR(stateP, "Soft target gamma is not a number convertible to double.\n");
                    exit(-1);
                }
            }
        }
    }
    return true;
}

FitnessP MultipleClassEvalOp::evaluate(IndividualP individual)
{
    FitnessP fitness(new FitnessMax);
    cartesian::Cartesian* cartesian = (cartesian::Cartesian*) individual->getGenotype().get();

    std::vector<std::vector<uint> > confusionMatrix(numberOfDifferentClasses, std::vector<uint>(numberOfDifferentClasses,0));

    for(uint i = 0; i < trainingInput.size(); i++) {
        std::vector<double> results;
        cartesian->evaluate(trainingInput[i], results);
        confusionMatrix[utility::vectorArgmax(results)][trainingOutputs[i]]++;
    }
    double valueOfFitness = utility::returnConfusionMatrixResult(confusionMatrix,trainingInput.size(),measureUsed);
    fitness->setValue(valueOfFitness);
    return fitness;
}
